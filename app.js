const express = require('express');


const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')



const authRoutes = require('./routes/auth');
const usersRoutes = require('./routes/users');
const platformRoutes = require('./routes/platforms');
const templatesRoutes = require('./routes/templates');
const sitesRoutes = require('./routes/sites');
const serversRoutes = require('./routes/servers');
const modulesRoutes = require('./routes/modules');
const jobsRoutes = require('./routes/jobs');

const uploadDatabase = require('./routes/files/database.js');
const uploadPackage = require('./routes/files/deploy-package.js');
const uploadsingleFile = require('./routes/files/deploy-byfiles.js')

app.use(morgan('dev'))
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use(cookieParser())
app.disable('etag');

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    if( req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, PATCH, DELETE, OPTIONS');
        return res.status(200).json({});
    }
    next();
})


app.use('/api/auth', authRoutes);
app.use('/api/users', usersRoutes);
app.use('/api/platforms', platformRoutes);
app.use('/api/templates', templatesRoutes);
app.use('/api/sites', sitesRoutes);
app.use('/api/servers', serversRoutes);
app.use('/api/modules', modulesRoutes);
app.use('/api/jobs', jobsRoutes);

app.use('/api/uploads/database/', uploadDatabase);
app.use('/api/uploads/package/', uploadPackage);
app.use('/api/uploads/files/', uploadsingleFile);

app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error)
});

app.use((error,req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    })
})

module.exports = app
