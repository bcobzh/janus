
var nano = require('../models/couchdb');

nano.db.create('servers').then((body) => {
    console.log('database servers created');
}).catch((err) => {
    console.log('database servers error: ' + err.reason)

})
let servers_views = {
    "_id": "_design/portail",
    "language": "javascript",
    "views": {
        "list": {
            "map": "function (doc) { emit(doc.name, {name: doc.name});}"
        }
    },
}

servers = nano.use('servers');
servers.get('_design/portail')
    .then(body => {
        servers_views._rev = body._rev;
        servers.insert(servers_views)
            .then(data => { })
            .catch(err => {console.log(err)});
    })
    .catch(error => {
        servers.insert(servers_views, '_design/portail')
            .then(data => {
            })
            .catch(err => {console.log(err)});
    });
