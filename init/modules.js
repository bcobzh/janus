var nano = require('../models/couchdb');

nano.db.create('modules').then((body) => {
    console.log('database modules created');
}).catch((err) => {
    console.log('database modules error: ' + err.reason)

})

let modules_views = {
  "_id": "_design/portail",
  "language": "javascript",
  "views": {
    "list": {
      "map": "function (doc) { emit(doc.name, {name: doc.name, active: doc.active, isAction: doc.isAction});}"
    },
    "actives" : {
       "map":  "function(doc) {if(doc.active) emit(doc.name, {name: doc.name, isAction: doc.isAction, cmds: doc.cmds});}"
    }
  },
}

modules = nano.use('modules');
modules.get('_design/portail')
    .then(body => {
        modules_views._rev = body._rev;
        modules.insert(modules_views)
            .then(data => { })
            .catch(err => {console.log(err)});
    })
    .catch(error => {
        modules.insert(modules_views, '_design/portail')
            .then(data => {
            })
            .catch(err => {console.log(err)});
    });
