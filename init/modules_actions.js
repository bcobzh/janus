var nano = require('../models/couchdb');
const yaml = require('js-yaml');
const fs   = require('fs');
var _ = require('lodash');

const modules = nano.use('modules');


var files = fs.readdirSync('init/modules/');
console.log(files);

for (var i=0; i < files.length;  i++){
    var doc = yaml.safeLoad(fs.readFileSync('init/modules/' + files[i], 'utf8'));
    let mod = doc.module;
    console.log(mod.name)
    mod.isAction = true;
    modules.get(mod.name)
        .then(body => { 
            console.log('old :' + mod.name)
            mod._rev = body._rev;
            modules.insert(mod)
                .then(data => {console.log(mod.name + ": updated")})
                .catch(err => {console.log(mod.name  + ": error")});
        }).catch(err => {
            console.log('new :' + mod.name)
            modules.insert(mod, mod.name)
                .then(data => {console.log(mod.name + ": updated")})
                .catch(err => {console.log(mod.name  + ": error")});
        })
}
