
var nano = require('../models/couchdb');

nano.db.create('jobs').then((body) => {
    console.log('database jobs created');
}).catch((err) => {
    console.log('database jobs error: ' + err.reason)

})

let jobs_views = {
    "_id": "_design/portail",
    "language": "javascript",
    "views": {
        "list": {
            "map": "function (doc) { emit(doc.name, doc);}"
        },
        "toQueue": {
            "map": `function (doc) {
                       if (doc.state == 0) {
                           emit(doc.name, doc)
                       } 
                   }`
        }  
    },
}



jobs = nano.use('jobs');
jobs.get('_design/portail')
    .then(body => {
        jobs_views._rev = body._rev;
        jobs.insert(jobs_views)
            .then(data => { })
            .catch(err => {console.log(err)});
    })
    .catch(error => {
        jobs.insert(jobs_views, '_design/portail')
            .then(data => {
            })
            .catch(err => {console.log(err)});
    });
