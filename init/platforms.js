var nano = require('../models/couchdb');

nano.db.create('platforms').then((body) => {
    console.log('database platforms created');
}).catch((err) => {
    console.log('database platforms error: ' + err.reason)

})
let platforms_views = {
    "_id": "_design/portail",
    "language": "javascript",
    "views": {
        "list": {
            "map": "function (doc) { emit(doc.name, {name: doc.name});}"
        }
    },
}

platforms = nano.use('platforms');
platforms.get('_design/portail')
    .then(body => {
        platforms_views._rev = body._rev;
        platforms.insert(platforms_views)
            .then(data => { })
            .catch(err => {console.log(err)});
    })
    .catch(error => {
        platforms.insert(platforms_views, '_design/portail')
            .then(data => {
            })
            .catch(err => {console.log(err)});
    });
