var nano = require('../models/couchdb');

nano.db.create('users').then((body) => {
    console.log('database users created');
}).catch((err) => {
    console.log('database users error: ' + err.reason)

})
let users_views = {
    "_id": "_design/portail",
    "language": "javascript",
    "views": {
        "list": {
            "map": "function (doc) { emit(doc.name, {name: doc.name, login: doc.login});}"
        }
    },
}

users = nano.use('users');
users.get('_design/portail')
    .then(body => {
        users_views._rev = body._rev;
        users.insert(users_views)
            .then(data => { })
            .catch(err => {console.log(err)});
    })
    .catch(error => {
        users.insert(users_views, '_design/portail')
            .then(data => {
            })
            .catch(err => {console.log(err)});
    });

