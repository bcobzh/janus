var nano = require('../models/couchdb');

nano.db.create('sites').then((body) => {
    console.log('database sites created');
}).catch((err) => {
    console.log('database sites error: ' + err.reason)

})
let sites_views = {
    "_id": "_design/portail",
    "language": "javascript",
    "views": {
        "list": {
            "map": "function (doc) { emit(doc.name, {name: doc.name, url: doc.url, template: doc.template});}"
        }
    },
}

sites = nano.use('sites');
sites.get('_design/portail')
    .then(body => {
        sites_views._rev = body._rev;
        sites.insert(sites_views)
            .then(data => { })
            .catch(err => {console.log(err)});
    })
    .catch(error => {
        sites.insert(sites_views, '_design/portail')
            .then(data => {
            })
            .catch(err => {console.log(err)});
    });
