var nano = require('../models/couchdb');

nano.db.create('templates').then((body) => {
    console.log('database templates created');
}).catch((err) => {
    console.log('database templates error: ' + err.reason)
})

let templates_views = {
  "_id": "_design/portail",
  "language": "javascript",
  "views": {
    "list": {
      "map": "function (doc) { emit(doc.name, {name: doc.name, platform: doc.platform});}"
    },
    "getAmods": {
       "map": "function(doc) {if(doc.mods) emit(doc.name, {name: doc.name, mods: doc.mods});}" 
     }
  }
}

templates = nano.use('templates');
templates.get('_design/portail')
    .then(body => {
        templates_views._rev = body._rev;
        templates.insert(templates_views)
            .then(data => { })
            .catch(err => {console.log(err)});
    })
    .catch(error => {
        templates.insert(templates_views, '_design/portail')
            .then(data => {
            })
            .catch(err => {console.log(err)});
    });
