const nano = require('../models/couchdb');


const databases = ["platforms",
                   "sites",
                   "users",
                   "templates",
                   "servers",
                   "jobs",
                   "modules"];

databases.forEach( db => {

nano.db.create(db).then((body) => {
      console.log('database ' + db  + ' created');
    }).catch((err) => {
        console.log('database ' + db  + ': ' + err.reason)

    })
})

let sites_views = {
  "_id": "_design/portail",
  "language": "javascript",
  "views": {
    "list": {
      "map": "function (doc) { emit(doc.name, {name: doc.name, url: doc.url, template: doc.template});}"
    }
  },
}

sites = nano.use('sites');
sites.get('_design/portail')
    .then(body => {
        sites_views._rev = body._rev;
        sites.insert(sites_views)
            .then(data => { })
            .catch(err => {console.log(err)});
    })
    .catch(error => {
        sites.insert(sites_views, '_design/portail')
            .then(data => {
            })
            .catch(err => {console.log(err)});
    });

console.log('++++++++++++++++++++++++++++++++++++++++++++')

let templates_views = {
  "_id": "_design/portail",
  "language": "javascript",
  "views": {
    "list": {
      "map": "function (doc) { emit(doc.name, {name: doc.name, platform: doc.platform});}"
    },
    "getAmods": {
       "map": "function(doc) {if(doc.mods) emit(doc.name, {name: doc.name, mods: doc.mods});}" 
     }
  }
}

templates = nano.use('templates');
templates.get('_design/portail')
    .then(body => {
        templates_views._rev = body._rev;
        templates.insert(templates_views)
            .then(data => { })
            .catch(err => {console.log(err)});
    })
    .catch(error => {
        templates.insert(templates_views, '_design/portail')
            .then(data => {
            })
            .catch(err => {console.log(err)});
    });

let modules_views = {
  "_id": "_design/portail",
  "language": "javascript",
  "views": {
    "list": {
      "map": "function (doc) { emit(doc.name, {name: doc.name, active: doc.active, isAction: doc.isAction});}"
    },
    "actives" : {
       "map":  "function(doc) {if(doc.active) emit(doc.name, {name: doc.name, isAction: doc.isAction, cmds: doc.cmds});}"
    }
  },
}

modules = nano.use('modules');
modules.get('_design/portail')
    .then(body => {
        modules_views._rev = body._rev;
        modules.insert(modules_views)
            .then(data => { })
            .catch(err => {console.log(err)});
    })
    .catch(error => {
        modules.insert(modules_views, '_design/portail')
            .then(data => {
            })
            .catch(err => {console.log(err)});
    });
let jobs_views = {
  "_id": "_design/portail",
  "language": "javascript",
  "views": {
    "list": {
      "map": "function (doc) { emit(doc.name, doc);}"
    },
  },
}

jobs = nano.use('jobs');
jobs.get('_design/portail')
    .then(body => {
        jobs_views._rev = body._rev;
        jobs.insert(jobs_views)
            .then(data => { })
            .catch(err => {console.log(err)});
    })
    .catch(error => {
        jobs.insert(jobs_views, '_design/portail')
            .then(data => {
            })
            .catch(err => {console.log(err)});
    });



let users_views = {
  "_id": "_design/portail",
  "language": "javascript",
  "views": {
    "list": {
      "map": "function (doc) { emit(doc.name, {name: doc.name, email: doc.email, roles: doc.roles} );}"
    },
  },
}

users = nano.use('users');
users.get('_design/portail')
    .then(body => {
        users_views._rev = body._rev;
        users.insert(users_views)
            .then(data => { })
            .catch(err => {console.log(err)});
    })
    .catch(error => {
        users.insert(users_views, '_design/portail')
            .then(data => {
            })
            .catch(err => {console.log(err)});
    });
