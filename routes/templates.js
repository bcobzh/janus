const express = require('express');
const router = express.Router();

const templatesController = require('../controllers/templates');

router.get('/amods', templatesController.getAMods)
router.post('/', templatesController.create)
router.get('/:name/envs', templatesController.getEnvs)
router.get('/:name', templatesController.getByName)
router.put('/:name', templatesController.templateUpdate)
/*
router.patch('/:templateId', templatesController.templateMod)
router.delete('/:templateId', templatesController.templateDel)
*/
router.get('/', templatesController.list)


module.exports = router;
