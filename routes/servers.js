const express = require('express');
const router = express.Router();
const serversController = require('../controllers/servers');

router.get('/', serversController.list)
router.get('/:name', serversController.getByName)
router.post('/', serversController.create)
router.delete('/:name', serversController.del)
router.put('/:name', serversController.serverUpdate)
router.patch('/:name', serversController.serverMod)

module.exports = router;
