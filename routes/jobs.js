const express = require('express');
const router = express.Router();
const jobsController = require('../controllers/jobs');
const actionsController = require('../controllers/actions');
const checkAuth = require('../middleware/check-auth');

router.get('/', jobsController.getAll)
//router.get('/search', jobsController.search)
router.get('/:jobId', jobsController.getById)
router.post('/', checkAuth, actionsController.addJob)
//router.patch('/:jobId', jobsController.jobMod)
//router.delete('/:jobId', jobsController.jobDel)

module.exports = router;
