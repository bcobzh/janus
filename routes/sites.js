const express = require('express');
const _ = require('lodash');

const sitesController = require('../controllers/sites.js');
const router = express.Router();

router.get('/', sitesController.list)
router.get('/:name', sitesController.getByName)
router.post('/', sitesController.create) 
router.put('/:name', sitesController.siteUpdate)
router.get('/:name/vars', sitesController.getVars)
router.get('/:name/tree', sitesController.getTree)
router.get('/:name/ftp-files', sitesController.ftpFiles)
router.get('/:name/crontab', sitesController.getCrontab)
router.put('/:name/crontab', sitesController.updateCrontab)


module.exports = router;
