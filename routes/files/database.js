const express = require('express');
const _ = require('lodash')
const fs = require('fs');

const multer = require('multer')

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        const subdir = 'uploads/database/' + req.params.siteName;
        if(!fs.existsSync(subdir)){
            fs.mkdirSync(subdir, {recursive: true})
        }
        cb(null, subdir);
    },
    filename: function(req, file, cb){
        cb(null, file.originalname)
    }
})
const fileFilter = (req, file, cb) => {
    if( file.mimetype.match(/application\/(sql|zip|gzip)/) ){
        cb(null, true);
    } else{
        cb(new Error('mimetype have to be "application/[sql|zip|gzip]" ') , false)
    }
}

const upload = multer({
    storage: storage,
    limits: {fileSize: 1024 * 1024 * 3},
    fileFilter: fileFilter
})

const router = express.Router();

router.post("/:siteName", upload.single('file'),(req, res, next) => {
    console.log(req.file)
    const urlPath = '/api/uploads/database/' + req.params.siteName + '/' + req.file.filename;
    response = {
        name: req.file.filename,
        url: urlPath,
        methods : ['GET', 'DELETE']
    }
    res.status(200).json(response)
})

router.get("/:siteName", (req, res, next) => {
    const deposit = 'uploads/database/' + req.params.siteName;
    fileList = [];
    if (fs.existsSync(deposit)) {
        fs.readdir(deposit, (err, items) => {
            for (var i=0; i<items.length; i++) {
                data = {
                    name: items[i],
                    url: '/api/uploads/database/' + req.params.siteName + '/' + items[i],
                    methods: ['GET', 'DELETE']
                }
                fileList.push(data)
            }
            res.status(200).json({files: fileList})
        })
    }else{
        res.status(200).json({files: []})
    }
})

router.get("/:siteName/:fileName", (req, res, next) => {
    const path = 'uploads/database/' + req.params.siteName + '/' + req.params.fileName;
    if(fs.existsSync(path)){
        res.download(path);
    }else{
        res.status(404).json({error: "file not found"})
    }
})
router.delete("/:siteName/:fileName", (req, res, next) => {
    const path = 'uploads/database/' + req.params.siteName + '/' + req.params.fileName;
    if(fs.existsSync(path)){
        fs.unlink(path, (err) => {
           if (err) {
               res.status(500).json({error: err})
            }else{
                res.status(200).json({message: 'file deleted'})
            }
        })
    }else {
        res.status(200).json({message: 'but file not found'})
    }

})


module.exports = router;
