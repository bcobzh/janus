const express = require('express');
const _ = require('lodash');

const usersController = require('../controllers/users.js');
const router = express.Router();

router.get('/', usersController.list)
router.get('/:email', usersController.getByName)
router.post('/', usersController.create) 
router.put('/:email', usersController.userUpdate) 


module.exports = router;
