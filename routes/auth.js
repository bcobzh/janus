const express = require('express');
const checkAuth = require('../middleware/check-auth');
const authContoller = require('../controllers/auth')
const router = express.Router();

router.post('/login', authContoller.login)
router.get('/user', checkAuth, authContoller.userParams)
router.post('/logout', (req, res, next) => {
    res.json({
        status: 'OK'
    })
})


module.exports = router;
