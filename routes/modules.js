const express = require('express');
const _ = require('lodash')

const router = express.Router();
const adminOnly = require('../middleware/admin-only');
const modulesController = require('../controllers/modules');

router.get('/', modulesController.list)
router.get('/actives', modulesController.getActives)
router.post('/', modulesController.create)
router.get('/:name', modulesController.getByName)
router.put('/status', modulesController.updateActives)
//router.put('/:moduleName', modulesController.moduleUpdate)
//router.put('/', modulesController.modulesStatus)
//router.patch('/:moduleName', modulesController.moduleMod)
//router.delete('/:moduleName', modulesController.moduleDel)

module.exports = router;
