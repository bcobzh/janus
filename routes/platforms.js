const express = require('express');
const _ = require('lodash');

const platformsController = require('../controllers/platforms.js');
const router = express.Router();
const {rootOnly, atLeastAdmin} =  require('../middleware/check-role.js');
const checkAuth = require('../middleware/check-auth');

router.get('/', checkAuth, platformsController.list)
router.get('/:name', checkAuth, atLeastAdmin, platformsController.getByName)
router.post('/', checkAuth, rootOnly, platformsController.create) 
router.put('/:name', checkAuth, platformsController.Update) 


module.exports = router;
