
var celery = require('node-celery'),
    client = celery.createClient({
        CELERY_BROKER_URL: 'amqp://hephaistos:secr3t@localhost:5672//',
        CELERY_RESULT_BACKEND: 'amqp://hephaistos:secr3t@localhost:5672//'
    });
 
client.on('error', function(err) {
    console.log(err);
});
 
client.on('connect', function() {
    client.call('tasks.from_node', ['ma commande python'], function(result) {
        console.log(result);
        client.end();
    });
});
