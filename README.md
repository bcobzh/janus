## Janus

Janus is the REST API of the Sisyphe project

It's a nodejs project and use couchDB

### Quick install with couchDBi during the dev

To run couchDB in dev, you can use _podman_ on Gnu/Linux

```bash
mkdir -p $HOME/volumes/couchdb
podman pod create -n sisyphe --publish 5984:5984
podman run -d --name thot  --pod sisyphe --env-file=couchdb.env  -v $HOME/volumes/couchdb:/opt/couchdb/data:Z  couchdb
```

To set the admin / password, Go to the page : [admin page](http://127.0.0.1:5984/_utils#setup)

then create the access file for the node app :

```sh
cp models/couchdb-model.js models/couchdb.js
# then update the password in couchdb.js
```

### Init databases

create the databases and views

```sh
node init/dbs.js
```

### start the app

```
nodemod server.js
```

