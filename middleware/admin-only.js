const jwt = require('jsonwebtoken');

const tokenInCookies = cookies => {
    console.log(cookies)
    try {
        if('jwtToken' in cookies){
            return cookies.jwtToken
        }else{
            console.log(cookies)
            return cookies['auth._token.local'].split(" ")[1]
        }
    }catch(error){
        console.log(error)
        return ""
    }
}

module.exports = (req, res, next) => {
    try {
        const token = 'authorization' in req.headers? req.headers.authorization.split(" ")[1]: tokenInCookies(req.cookies)
        const decoded = jwt.verify(token, process.env.JWT_KEY);
        console.log(decoded);
        if(!('admin' in decoded.roles)){
            return res.status(404).json({
                message: "Not found"
            })
        }
        req.userData = decoded;
        next()
    } catch(error){
        console.log(error);
        return res.status(401).json({
            message: "Auth failed"
        });
    };
}
