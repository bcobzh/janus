const _ = require('lodash')

exports.rootOnly = (req, res, next) => {
        console.log(req.userData)
    try {
        if(req.userData.scope.indexOf('root') < 0){
           return res.status(403).json({message: "Forbidden: insuffisant right"})
        }else{
            next()
        }
    } catch(error){
        return res.status(403).json({
            message: "Forbidden"
        });
    };
}
exports.atLeastAdmin = (req, res, next) => {
    try {
        if(_.intersection(req.userData.scope, ['root', 'admin'] ) < 0){
           return res.status(403).json({message: "Forbidden: insuffisant right"})
        }else{
            next()
        }
    } catch(error){
        return res.status(403).json({
            message: "Forbidden"
        });
    };
}
