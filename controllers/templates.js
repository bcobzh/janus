const nano = require('../models/couchdb');
const R = require('ramda');
const templates = nano.use('templates');
const platforms = nano.use('platforms');

const checkAuth = require('../middleware/check-auth');

exports.create = (req, res, next) => {
    data = req.body;
    templates.insert(data, data.name)
       .then(body => {
           res.status(200).json({message: "template created", name: body.id})
        }).catch(err => {
            res.status(err.statusCode).json({error: err.reason})
        })
};

exports.list = (req, res, next) => {
    templates.view('portail', 'list')
      .then((body) => { 
          let retBody = body.rows.map(row => {
              console.log(row.value)
              return {'name': row.value.name,
                      'platform': row.value.platform
                     }
          });
          res.status(200).json(retBody)
    })
    .catch(err => { 
        res.status(err.statusCode).json({error: err.reason})
    })
};

exports.getByName = (req, res, next) => {
    const name = req.params.name;
    if (name === undefined ){
        res.status(404).json({error: 'name in querySting not found' })
    } else {
    templates.get(name)
        .then(body => {
            console.log(body)
            if(body._id  === undefined ){
                res.status(404).json({error: 'not found'})
            }else{
              res.status(200).json(R.omit(['_id', '_id'], body))
            }
        })
        .catch(err => {
            res.status(err.statusCode).json({error: err.error})
        })
    }
};

exports.templateUpdate = (req, res, next) => {
    let name = req.params.name;
    templates.get(name)
        .then(data => {
            console.log(data);
            let new_data = req.body;
            new_data._rev = data._rev;
            templates.insert(new_data, name)
                .then(data => {
                    console.log(data);
                    res.status(200).json({message: "template updated"})
                })
                .catch(err => {
                    console.log(err);
                    res.status(err.statusCode).json({err })
                })

        })
        .catch(err => {
            console.log(err)
            res.status(501).json({message: "update error"})
        })
};

exports.search = (req, res, next) => {
    templateName = req.query.name;
    Template
        .findOne({name: templateName})
        .exec()
        .then(doc => {
            res.status(200).json(doc)
        })
        .catch(err => {
            console.log(error)
            res.status(500).json({error: err})
        })
};

exports.getAMods = (req, res, next) => {
    // get available modules for all templates
    console.log(req.query.name)
    templateName = R.pathOr('all', ['query', 'name'], req);

    templates.view('portail', 'getAmods')
        .then(docs => {
            console.log(docs)
            let allTpl = docs.rows.map(doc => {
                                return {name: doc.value.name,
                                        mods: R.keys(doc.value.mods)}
                               })
            if(templateName === 'all'){
                res.status(200).json(allTpl)
            }else{
                let ret = allTpl.find(tpl => { return tpl.name === templateName})
                console.log(ret)
                if(R.isNil(ret)){
                    res.status(200).json([])
                }else{
                    res.status(200).json(ret)
                }
            }
        })
        .catch(err => {
            res.status(200).json([])
        })
};

exports.getEnvs =(req, res, next) => {
    console.log(req.params)
    let name = req.params.name
    // get available modules for all templates
    templates.get(name)
        .then(doc => {
            let platformName = doc.platform;
            if(platformName === undefined){
                res.status(200).json([])
            }else{
                platforms.get(platformName)
                    .then(plt => {
                        let envs = plt.envs === undefined ? [] : plt.envs;
                        res.status(200).json(envs)
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(200).json([])
                    })
            }

        })
        .catch(err => {
            res.status(404).json({reason: "Not Found"})
        })
};

exports.templateDel =  (req, res,next) => {
    const id = req.params.templateId;
    Template
        .remove({_id: id})
        .exec()
        .then(result => {
            res.status(200).json(result)
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
            });
        });
};

