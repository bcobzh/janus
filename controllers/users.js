const nano = require('../models/couchdb');
const R = require('ramda')
const users = nano.use('users');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt')


exports.list = (req, res, next) => {
    console.log('test')
    users.view('portail', 'list')
        .then((body) => { 
            let retBody = body.rows.map(row => {
                return row.value
            });
            res.status(200).json(retBody)
        })
        .catch(err => { 
            res.status(err.statusCode).json({error: err.reason})
        })
}

exports.getByName =  (req, res, next) => {
    console.log(req.params)
    const email = req.params.email;
    if (email === undefined ){
        res.status(404).json({error: 'email in querySting not found' })
    } else {
        users.get(email)
            .then(body => {
                if(body  === undefined ){
                    res.status(404).json({error: 'not found'})
                }else{

                    res.status(200).json(R.omit(['_id', '_rev'], body))
                }
            })
            .catch(err => {
                res.status(500).json({error: err})
            })
    }
};


exports.create =  (req, res, next) => {
    data = req.body;
    console.log(req.body)
    console.log(req.body.password )
    if (req.body.password === undefined ) {
        users.insert(data, data.email)
            .then(body => {
                res.status(200).json({message: "User created without password"})
            }).catch(err => {
                res.status(err.statusCode).json({error: err.reason})
            })
    }else {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
            if (err) {
                return res.status(500).json({
                    error: err
                });
            } else {
                data.password = hash;
                users.insert(data, data.email)
                    .then(body => {
                        res.status(200).json({message: "User created"})
                    }).catch(err => {
                        res.status(err.statusCode).json({error: err.reason})
                    })
            }
        })
    }
};



exports.userUpdate = (req, res, next) => {
    let email = req.params.email;
    users.get(email)
        .then(data => {
            console.log(data);
            let new_data = req.body;
            new_data._rev = data._rev;
            users.insert(new_data, email)
                .then(data => {
                    console.log(data);
                    res.status(200).json({message: "user updated"})
                })
                .catch(err => {
                    console.log(err);
                    res.status(err.statusCode).json({err })
                })

        })
        .catch(err => {
            console.log(err)
            res.status(501).json({message: "update error"})
        })
};
