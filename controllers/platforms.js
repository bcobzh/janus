const nano = require('../models/couchdb');
const R = require('ramda');
const platforms = nano.use('platforms');


exports.create =  (req, res, next) => {
    platform = req.body;
   // console.log(plateform)
    platforms.insert(platform, platform.name)
       .then(body => {
           res.status(200).json(body)
        }).catch(err => {
            res.status(err.statusCode).json({error: err.reason})
        })
};

exports.list = (req, res, next) => {
    platforms.view('portail', 'list')
      .then((body) => { 
          console.log(body)
          let retBody = body.rows.map(row =>{
              return {'name': row.id,
                      'desc': row.desc,
                      'id': row.id,
                      'url': '/api/platforms/'  + row.id
                      }
          });
          res.status(200).json(retBody);
    })
    .catch(err => { 
        res.status(err.statusCode).json({error: err.reason})
    })
}

exports.getByName =  (req, res, next) => {
    const name = req.params.name;
    console.log(name)
    if (name === undefined ){
        res.status(404).json({error: 'name in querySting not found' })
    } else {
    platforms.get(name)
        .then(body => {
            if(body._id  === undefined ){
                res.status(404).json({error: 'not found'})
            }else{
              
              res.status(200).json(R.omit(['_id', '_rev'], body))
            }
        })
        .catch(err => {
            res.status(500).json({error: err})
        })
    }
};


exports.Update = (req, res, next) => {
    let name = req.params.name;
    platforms.get(name)
        .then(data => {
            console.log(data);
            let new_data = req.body;
            new_data._rev = data._rev;
            platforms.insert(new_data, name)
                .then(data => {
                    console.log(data);
                    res.status(200).json({message: "server updated"})
                })
                .catch(err => {
                    console.log(err);
                    res.status(err.statusCode).json({err })
                })

        })
        .catch(err => {
            console.log(err)
            res.status(501).json({message: "update error"})
        })
}
    

