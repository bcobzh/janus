const nano = require('../models/couchdb');
const R = require('ramda');
const servers = nano.use('servers');

exports.create = (req, res, next) => {
    console.log(req.body)
    servers.insert(req.body, req.body.name)
        .then(body => {
            res.status(200).json(body)
        }).catch(err => {
            res.status(err.statusCode).json({error: err.reason})
        })
};

exports.list = (req, res, next) => {
    servers.view('portail', 'list')
        .then((body) => { 
            console.log(body)
            let retBody = body.rows.map(row =>{
                return {'name': row.id,
                    'id': row.id,
                    'url': '/api/servers/'  + row.id
                }
            });
            res.status(200).json(retBody);
        })
        .catch(err => { 
            res.status(err.statusCode).json({error: err.reason})
        })
}

exports.getByName = (req, res, next) => {
    const name = req.params.name;
    console.log(name)
    if (name === undefined ){
        res.status(404).json({error: 'name in querySting not found' })
    } else {
        servers.get(name)
            .then(body => {
                if(body._id  === undefined ){
                    res.status(404).json({error: 'not found'})
                }else{

                    res.status(200).json(R.omit(['_id', '_rev'], body))
                }
            })
            .catch(err => {
                res.status(500).json({error: err})
            })
    }
};


exports.del = (req, res,next) => {
    let name = req.params.name;
    servers.get(name)
        .then(server => {
            if (server._rev === undefined){
                res.status(404).json({error: "server not find"})
            }else{
                servers.destroy(server._id, server._rev)
                    .then( body => {
                        res.status(200).json({message: "server deleted"})
                    })
                    .catch(err => {
                        console(err)
                        ret.status(500).json({error: "internal error"})
                    })
            }
        })
        .catch(err => {
            console.log(err)
            ret.status(500).json({error: "internal error"})
        })
}

exports.serverUpdate = (req, res, next) => {
    let name = req.params.name;
    servers.get(name)
        .then(data => {
            console.log(data);
            let new_data = req.body;
            new_data._rev = data._rev;
            servers.insert(new_data, name)
                .then(data => {
                    console.log(data);
                    res.status(200).json({message: "server updated"})
                })
                .catch(err => {
                    console.log(err);
                    res.status(err.statusCode).json({err })
                })

        })
        .catch(err => {
            console.log(err)
            res.status(501).json({message: "update error"})
        })
}


exports.serverMod = (req, res, next) => {
    const id = req.params.serverId;
    console.log(req.body);
    const upDateOps = {};
    for(const ops of req.body){
        upDateOps[ops.propName] = ops.value;
    }

    Server
        .update({_id: id}, {$set: upDateOps})
        .exec()
        .then(result => {
            res.status(200).json({result})
        })
        .catch(err => {
            res.status(500).json({error: err})
        });
};

