const nano = require('../models/couchdb');
const R = require('ramda');
const sites = nano.use('sites');
const templates = nano.use('templates');
const platforms = nano.use('platforms');
var exec = require('child_process').exec;

exports.create =  (req, res, next) => {
    data = req.body;
    sites.insert(data, data.url)
        .then(body => {
            res.status(200).json(body)
        }).catch(err => {
            res.status(err.statusCode).json({error: err.reason})
        })
};

exports.list = (req, res, next) => {
    templates.view('portail', 'list')
        .then(tpls => {
            let tpl_plt =  R.fromPairs(
                tpls.rows.map( x => {
                    return [x.value.name ,x.value.platform]
                })
            );
            sites.view('portail', 'list')
                .then((body) => { 
                    let retBody = body.rows.map(row => {
                        return {'name': row.value.name,
                            'url': row.value.url,
                            'template': row.value.template,
                            'platform': tpl_plt[row.value.template]
                        }
                    });
                    res.status(200).json(retBody)
                })
                .catch(err => { 
                    res.status(err.statusCode).json({error: err.reason})
                })
        })
        .catch(err =>{
            console.log(err)
            res.status(500).json({error: err})
        })


}

exports.getByName =  (req, res, next) => {
    const name = req.params.name;
    if (name === undefined ){
        res.status(404).json({error: 'name in querySting not found' })
    } else {
        sites.get(name)
            .then(body => {
                let ret =  body;
                if(ret  === undefined ){
                    res.status(404).json({error: 'not found'})
                }else{

                    res.status(200).json(R.omit(['_id', '_rev' ], ret))
                }
            })
            .catch(err => {
                console.log(err)
                res.status(500).json({error: err})
            })
    }
};


exports.siteUpdate = (req, res, next) => {
    let name = req.params.name;
    console.log(req.params);
    console.log(req.body);
    console.log('---------');
    sites.get(name)
        .then(data => {
            console.log(data);
            let new_data = req.body;
            new_data._rev = data._rev;
            sites.insert(new_data, name)
                .then(data => {
                    console.log(data);
                    res.status(200).json({message: "site updated"})
                })
                .catch(err => {
                    console.log(err);
                    res.status(err.statusCode).json({err })
                })

        })
        .catch(err => {
            console.log(err)
            res.status(501).json({message: "update error"})
        })
};

exports.create =  (req, res, next) => {
    data = req.body;
    sites.insert(data, data.name)
        .then(body => {
            res.status(200).json(body)
        }).catch(err => {
            res.status(err.statusCode).json({error: err.reason})
        })
};

const siteVars = ( name => {
    return new Promise ((resolve, reject) => {
        console.log(name)
        sites.get(name)
            .then(site => {
                templates.get(site.template)
                    .then(template => {
                        platforms.get(template.platform)
                            .then(platform => {
                                const tmpRet =  R.mergeDeepRight(platform.vars, template.vars, site.vars)
                                var ret = {}
                                R.keys(tmpRet).map(cmd => {
                                    ret[cmd] = {};
                                    const cmdObj = tmpRet[cmd]
                                    R.keys(cmdObj).map(env =>{
                                        try{
                                            ret[cmd][env] = JSON.parse(tmpRet[cmd][env])
                                        }catch(error){
                                            ret[cmd][env] = tmpRet[cmd][env]
                                        }
                                    })
                                })
                                resolve( ret )

                            }).catch(err => {
                                reject({status: 500, err: err})
                            });
                    }).catch(err => {
                        reject({status: 500, err: err})
                    });

            }).catch(err => {
                reject({status: 500, err: err})
            })
    })

})


exports.getVars = (req, res, next) => {
    const name = req.params.name;
    siteVars(name)
        .then(vars => {
            if (typeof(req.query.val) != 'undefined'){
                key = req.query.val
                res.status(200).json(R.pick([key], vars))
            }else{
                res.status(200).json(vars);
            }
        }).catch(error => {
            res.status(error.status).json(error.err)
        })
};
exports.getTree = (req, res, next) => {
    // content from tree  :
    // all  : tree --inodes --dirsfirst -Ji -I cache -L 10 --noreport  www
    const name = req.params.name;
    siteVars(name)
        .then(vars => {
            console.log(req.query)
            let dirOnly = req.query.dir_only;

            var celery = require('node-celery'),
                client = celery.createClient({
                    CELERY_BROKER_URL: 'amqp://hephaistos:secr3t@localhost:5672//',
                    CELERY_RESULT_BACKEND: 'amqp://hephaistos:secr3t@localhost:5672//',
                });

            client.on('error', function(err) {
                console.log(err);
                res.status(500).json({err})
            });
            let env = 'prod';

            client.on('connect', function() {
                client.call('tasks.tree4api', [name, env, dirOnly], function(result) {
                    var strTree = result.result.replace(/^\s+|\s+$/gm,'');
                    client.end();
                    console.log(strTree);
                    res.status(200).json(JSON.parse(strTree));
                });
            })
        }).catch(error => {
            res.status(error.status).json(error.err)
        })

};


exports.ftpFiles = (req, res, next) => {
    const name = req.params.name;
    siteVars(name)
        .then(vars => {
            console.log(vars);
            res.status(200).json(['fic1.sql', 'fic2.sql']);
        })
        .catch(error => {
            res.status(error.status).json(error.err)
        })
}

exports.getCrontab = (req, res, next) => {
    const name = req.params.name;
    sites.get(name)
            .then(body => {
                if(body === undefined ){
                    res.status(404).json({error: 'not found'})
                }else{

                    res.status(200).json(R.pathOr([], ['crontab'], body))
                }
            })
            .catch(err => {
                console.log(err)
                res.status(500).json({error: err})
            })
};

exports.updateCrontab = (req, res, next) => {
    let name = req.params.name;
    let crontab = req.body;
     console.log(name)
    console.log('---------');
    sites.get(name)
        .then(data => {
            console.log(data);
            var new_data = data;
            new_data.crontab = req.body;
            sites.insert(new_data, name)
                .then(data => {
                    console.log(data);
                    res.status(200).json({message: "crontab updated"})
                })
                .catch(err => {
                    console.log(err);
                    res.status(err.statusCode).json({err })
                })

        })
        .catch(err => {
            console.log(err)
            res.status(501).json({message: "update error"})
        })
};
