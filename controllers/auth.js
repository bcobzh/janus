const nano = require('../models/couchdb');
const users = nano.use('users');
const jwt = require('jsonwebtoken');                                            
const bcrypt = require('bcrypt')


exports.login = (req, res, next) => {
    let email = req.body.email;
    if( email === undefined) {
        res.status(404).json({error: "email required"})
    }else{
        users.get(email)
            .then(user => {
                console.log(user.password)
                bcrypt.compare(req.body.password, user.password, (error, resp) => {
                    if(error) {
                        return res.status(401).json({message: "Auth failed"})
                    }
                    if (resp) {
                        const roles = ['user', 'admin']; 
                        console.log(user)
                        const scope = roles.slice( 0, roles.indexOf(user.role) + 1);
                        const token = jwt.sign(
                            {
                                username: user.email,
                                userID: user._rev,
                                scope: scope
                            },
                            process.env.JWT_KEY,
                            {
                                expiresIn: "4h"
                            },
                        );
                        return res.status(200)
                            .cookie('jwtToken', token, { maxAge: 1000 * 60 * 60 * 4 })
                            .json({
                                message: 'Auth successful',
                                token: token
                            });
                    }
                    res.status(401).json({
                        message: "Auth failed"
                    })


            })
            })
            .catch(err => {
                console.log('auth error, in email get: ' + err.error);
                res.status(401).json({message: "Auth failed"})
            })
    }
}


exports.userParams = (req, res, next) => {
  res.json({ user: {login: req.userData.username,
                   scope: req.userData.scope}
            })
}
