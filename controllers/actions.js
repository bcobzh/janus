const nano = require('../models/couchdb');
const R = require('ramda')
const sites = nano.use('sites');
const templates = nano.use('templates');
const jobs = nano.use('jobs');
const nunjucks = require('nunjucks');
const celery = require('node-celery');

const checkAuth = require('../middleware/check-auth');


exports.addJob =  (req, res, next) => {

    const getOrMissing = R.pathOr('missing');

    const siteName = getOrMissing(['siteName'], req.body);
    const module = getOrMissing(['module'], req.body);
    const action = getOrMissing(['action'], req.body);
    const cmdPath = ['mods', module, action, 'run'];

    sites.get(siteName)
        .then(site => {
            templates.get(site.template)
                .then(template => {
                    if(R.hasPath(cmdPath, template)){
                        const jobId = Math.floor(Date.now() / 1000);
                        const form = req.body;
                        const  cmdTpl = template.mods[module][action].run;

                        const fullTPL = "/bin/env SITE=${form.siteName} ENV=${form.environment} JOBID=${jobId} " + cmdTpl
                        try{
                            const fullCmd = new Function('form, jobId', 'return `' + fullTPL + '`;')(form, jobId)
                            let computed = {
                                site: siteName,
                                script: nunjucks.renderString(fullCmd, {form: req.body}),
                                state: 0,
                                user: req.userData.username
                            }
                            let job = R.mergeDeepRight(req.body, computed)

                            jobs.insert(job, jobId.toString())
                                .then(ret => {
                                    res.status(200).json({ 
                                        jobUrl: '/jobs/' + jobId,
                                        state: 'in queue'
                                    })
                                })
                                .catch(err => {
                                    console.log(err)
                                    console.log(':-(((((')
                                    res.status(500).json( {error: err})
                                })
                        }catch(error){
                            console.log(error)
                            res.status(503).json( {error: error})
                        }
                    }else{
                        res.status(409).json( {error: 'no matching command found'})
                    }
                })
                .catch(tplErr => {
                    console.log(tplErr)
                    res.status(tplErr.satusCode).json({err: tplErr.reason})
                })
        })
        .catch(err => {
            console.log(err)
            console.log('++++++++++++++++++++++++')
            res.status(err.statusCode).json({err: err.reason})
        })
}
