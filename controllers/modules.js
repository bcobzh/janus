const nano = require('../models/couchdb');
const R = require('ramda');
const modules = nano.use('modules');

exports.create =  (req, res, next) => {
    module = req.body;
    modules.insertd(module, module.name)
       .then(body => {
           res.status(200).json(body)
        }).catch(err => {
            res.status(err.statusCode).json({error: err.reason})
        })
};

exports.list = (req, res, next) => {
    modules.view('portail', 'list')
      .then((body) => { 
          res.status(200).json(body.rows.map(row => {return row.value}));
    })
    .catch(err => { 
        res.status(err.statusCode).json({error: err.reason})
    })
}

exports.getActives = (req, res, next) => {
    modules.view('portail', 'actives')
      .then((body) => { 
          res.status(200).json(body.rows.map(row => {return row.value}));
    })
    .catch(err => { 
        res.status(err.statusCode).json({error: err.reason})
    })
};

exports.getByName =  (req, res, next) => {
    const name = req.params.name;
    if (name === undefined ){
        res.status(404).json({error: 'name in querySting not found' })
    } else {
    modules.get(name)
        .then(body => {
            let ret =  body;
            if(ret  === undefined ){
                res.status(404).json({error: 'not found'})
            }else{
              
              res.status(200).json(R.omit(['_id', '_rev'], ret))
            }
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({error: err})
        })
    }
};


exports.moduleUpdate = (req, res, next) => {
    let name = req.params.name;
    modules.get(name)
        .then(data => {
            console.log(data);
            let new_data = req.body;
            new_data._rev = data._rev;
            sites.insert(new_data, name)
                .then(data => {
                    console.log(data);
                    res.status(200).json({message: "site updated"})
                })
                .catch(err => {
                    console.log(err);
                    res.status(err.statusCode).json({err })
                })

        })
        .catch(err => {
            console.log(err)
            res.status(501).json({message: "update error"})
        })
};


exports.updateActives =  (req, res, next) => {
    console.log(req.body);
    let ret = true;
    for (let key in req.body){
        console.log(key)
        modules.get(key)
            .then(data => {
              data.active = req.body[key];
              modules.insert(data, key)
                .then(ok => {})
                .catch(err => {ret = false})
            })
            .catch(err => {
                console.log('module ' + key + ' not found')
            })
    }
    res.status(200).json({});
};

