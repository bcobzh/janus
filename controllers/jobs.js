const nano = require('../models/couchdb');
const R = require('ramda');
const jobs = nano.use('jobs');


exports.getAll = (req, res, next) => {
    const days = (typeof req.query.days === 'undefined') ? 15: req.query.days;
    const since  = ~~(new Date().getTime()/1000) - (days * 24 * 3600);
    jobs.view('portail','list')
        .then(body => {
            res.status(200).json(body.rows.map(row => {return row.value}))
        })
        .catch(err => {
            res.status(err.statusCode).json({error: err.reason})
        })
};

exports.getById = (req, res, next) => {
    const id = R.pathOr('0', ['params', 'jobId'], req);
    jobs.get(id)
      .then(body => {
          res.status(200).json(body)
      })
      .catch(err => {
          res.status(err.statusCode).json({error: err.reason})
      })
}

exports.getNewOnes = (req, res, next) => {
    const days = (typeof req.query.days === 'undefined') ? 15: req.query.days;
    const since  = ~~(new Date().getTime()/1000) - (days * 24 * 3600);
    jobs.view('portail','toQueue')
        .then(body => {
            res.status(200).json(body.rows.map(row => {return row.value}))
        })
        .catch(err => {
            res.status(err.statusCode).json({error: err.reason})
        })
};
