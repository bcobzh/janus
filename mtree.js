//your object
var tree = [{"type":"directory",
             "name":"www",
             "contents":[{"type":"directory",
                          "name":"asset",
                          "inode":253322,
                          "contents":[]},
                         {"type":"directory",
                          "name":"import",
                          "inode":253323,
                          "contents":[{"type":"file",
                                       "name":"test.csv",
                                       "inode":251486}]},
                         {"type":"directory",
                          "name":"static",
                          "inode":253321,
                          "contents":[{"type":"file",
                                       "name":"app.css",
                                       "inode":252353
                                     },
                                     {"type":"file",
                                      "name":"app.js",
                                      "inode":252347}]
                        },
                        {"type":"file",
                         "name":"index.html",
                        "inode":251466}]}]


function traverse(tree, target, path) {
    if(tree.inode == target){
        return path 
    }
    if (tree.type != 'file' && tree.contents.length > 0) {
        
        tree.contents.forEach( function (node)  {
            ret = traverse(node, target, path + '/' + node.name) ;
            if (ret !== undefined){
                console.log(ret)
                return ret
            }
        })
    }

}





console.log(traverse(tree[0], 252353, tree[0].name));
